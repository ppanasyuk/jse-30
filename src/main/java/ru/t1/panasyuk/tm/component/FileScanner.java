package ru.t1.panasyuk.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.panasyuk.tm.command.AbstractCommand;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public final class FileScanner extends Thread {

    @NotNull
    private final Bootstrap bootstrap;

    @NotNull
    private final List<String> commands = new ArrayList<>();

    @NotNull
    private final File folder = new File("./");

    public FileScanner(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
        setDaemon(true);
    }

    public void init() {
        @NotNull final Iterable<AbstractCommand> commands = bootstrap.getCommandService().getCommandsWithArgument();
        commands.forEach(e -> this.commands.add(e.getName()));
        start();
    }

    @SneakyThrows
    @Override
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            Thread.sleep(3000);
            for (@NotNull final File file : folder.listFiles()) {
                if (file.isDirectory()) continue;
                @NotNull final String fileName = file.getName();
                final boolean check = commands.contains(fileName);
                if (check) {
                    try {
                        file.delete();
                        bootstrap.processCommand(fileName);
                    } catch (@NotNull final Exception e) {
                        bootstrap.getLoggerService().error(e);
                    }
                }
            }
        }
    }

}