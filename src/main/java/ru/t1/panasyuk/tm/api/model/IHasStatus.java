package ru.t1.panasyuk.tm.api.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.panasyuk.tm.enumerated.Status;

public interface IHasStatus {

    @NotNull
    Status getStatus();

    void setStatus(@NotNull Status status);

}