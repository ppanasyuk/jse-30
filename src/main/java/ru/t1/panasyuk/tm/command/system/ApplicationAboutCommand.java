package ru.t1.panasyuk.tm.command.system;

import org.jetbrains.annotations.NotNull;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    @NotNull
    private static final String ARGUMENT = "-a";

    @NotNull
    private static final String DESCRIPTION = "Show developer info.";

    @NotNull
    private static final String NAME = "about";

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("Name: " + getPropertyService().getAuthorName());
        System.out.println("E-mail: " + getPropertyService().getAuthorEmail());
        System.out.println("Application Name: " + getPropertyService().getApplicationName());

        System.out.println("[GIT]");
        System.out.println("Branch: " + getPropertyService().getGitBranch());
        System.out.println("Commit Id: " + getPropertyService().getGitCommitId());
        System.out.println("Committer: " + getPropertyService().getGitCommitterName());
        System.out.println("E-mail: " + getPropertyService().getGitCommitterEmail());
        System.out.println("Message: " + getPropertyService().getGitCommitMessage());
        System.out.println("Time: " + getPropertyService().getGitCommitTime());
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

}