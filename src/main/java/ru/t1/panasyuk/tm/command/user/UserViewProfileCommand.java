package ru.t1.panasyuk.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.panasyuk.tm.enumerated.Role;
import ru.t1.panasyuk.tm.model.User;

public final class UserViewProfileCommand extends AbstractUserCommand {

    @NotNull
    private final String NAME = "user-view-profile";

    @NotNull
    private final String DESCRIPTION = "View profile of current user.";

    @Override
    public void execute() {
        @NotNull final User user = getAuthService().getUser();
        System.out.println("[USER VIEW PROFILE]");
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("E-MAIL: " + user.getEmail());
        System.out.println("ROLE: " + user.getRole().getDisplayName());
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}