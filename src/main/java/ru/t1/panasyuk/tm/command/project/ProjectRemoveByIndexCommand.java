package ru.t1.panasyuk.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.exception.entity.ProjectNotFoundException;
import ru.t1.panasyuk.tm.model.Project;
import ru.t1.panasyuk.tm.util.TerminalUtil;

public final class ProjectRemoveByIndexCommand extends AbstractProjectCommand {

    @NotNull
    private static final String DESCRIPTION = "Remove project by index.";

    @NotNull
    private static final String NAME = "project-remove-by-index";

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final String userId = getUserId();
        getProjectTaskService().removeProjectByIndex(userId, index);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}